class Nanoc::CompilationItemView
  def slug
    File.basename(identifier, '.md')
  end
end

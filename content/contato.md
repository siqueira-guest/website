---
title: Contato
---

# Contato com o ICTL

Para assuntos gerais, escreva para [contato@ictl.org.br](mailto:contato@ictl.org.br)

Para assuntos financeiros, escreva para [financeiro@ictl.org.br](mailto:financeiro@ictl.org.br)


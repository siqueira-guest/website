---
title: Membros
---

# Membros do ICTL

* Adriana Cássia da Costa (Conselheira Fiscal)
* Antonio Carlos da Conceição Marques
* Antonio Soares de Azevedo Terceiro  (Presidente)
* Cleber de Oliveira Campos Barco Ianes (Secretário)
* Daniel Lenharo de Souza (Conselheiro Fiscal)
* Giovani Augusto Ferreira
* Leonardo Rodrigues Pereira (Conselheiro Fiscal)
* Lucas Kanashiro Duarte
* Paulo Henrique de Lima Santana (Tesoureiro)
* Thiago Faria Mendonça
* Valéssio Soares de Brito

---
title: Sobre o ICTL
---

# ICTL

O ICTL -- Instituto para Conservação de Tecnologias Livres -- é uma associação
com sede na cidade de Curitiba/PR. Seus objetivos estatutários são:

* Incentivo ao uso e desenvolvimento de Software Livre e Hardware
  Livre;
* Apoio e divulgação de projetos de Software Livre e Hardware Livre ;
* Apoio a projetos de inclusão digital realizados com Software Livre;
* Realização de eventos, cursos, campanhas e demais atividades de
  cunho técnico, filosófico, social e educacional, relacionados a
  Software Livre;
* Produção de materiais impressos, digitais e multimídia referentes a
  Software Livre, e apoio a produções realizadas por terceiros;
* Produção de artigos de vestuário, acessórios e brindes referentes a
  Software Livre;
* Incentivo a adoção de Software Livre pelos governos municipais,
  estaduais e federal;
* Apoio a outras entidades nacionais e internacionais que tenham
  finalidades comuns relacionadas a Software Livre.

Veja também:

- [estatuto do ICTL](/estatuto/).
- [Membros](/membros/).

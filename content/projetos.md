---
title: Projetos
---

# Projetos membros do ICTL

## DebConf 19

![Logo da DebConf 19](/projetos/debconf19.png)

A [19ª edição da DebConf](https://debconf19.debconf.org/), a conferência anual
da comunidade Debian, acontece em Curitiba entre 14 e 28 de Julho de 2019. Além
de uma programação completa com palestras técnicas, sociais e políticas, a
DebConf cria uma oportunidade para desenvolvedores(as), contribuidores(as) e
outras pessoas interessadas se conhecerem presencialmente e trabalharem juntas
de maneira mais próxima

## Linux Developer Conference Brazil

![Logo da DebConf 19](/projetos/linuxdev-br.png)

A [Linux Developer Conference Brazil](https://linuxdev-br.net/) é uma
conferência internacional focado na discussão de projetos de infra-estrutura do
mundo do Software Livre como Linux (o kernel), systemd, containers, wayland,
Gstreamer, gcc, llvm, MESA, gdb, bootloaders, qemu, kvm, ChromeOS, tracing,
segurança, e outros componentes de baixo nível.
